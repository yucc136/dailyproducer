package com.yuccooo.producer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.yucc.data.db.FolderItem;

public class DailyProducer {
	
	private Gson gson;
	
	private List<Object> data;
	private List<FolderItem> folderItems ;
	
	public DailyProducer() {
		// TODO Auto-generated constructor stub
		gson = getCustizedGson();
		data = new LinkedList<Object>();
		folderItems = new LinkedList<FolderItem>();
		data.add(folderItems);
	}
	
	private Gson getCustizedGson() {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
				.setPrettyPrinting().create();
	}
	
	public void addItem(Object item) {
		data.add(0,item);
	}
	
	public String toJson() {
		return gson.toJson(data);
	}
	
	@Override
	public String toString() {
		return toJson();
	}
	
	public void getUpdatedTime(String json){
		System.out.println("updated time: " + gson.fromJson(json, String.class));
	}
	
	public List<FolderItem> fromJson(JsonElement json) {
		// json转为带泛型的list
		List<FolderItem> result = gson.fromJson(json,
				new TypeToken<List<FolderItem>>() {
				}.getType());
		for (FolderItem stu : result) {
			System.out.println(stu);
		}
		return result;
	}
	
	public FolderItem fromJsonItem(JsonElement json) {
		// json转为带泛型的list
		FolderItem result = gson.fromJson(json,
				new TypeToken<FolderItem>() {
				}.getType());
			System.out.println(result);
		return result;
	}
	
	public void addItemFromCategoryAndPage(String category, int page, int name) {
		FolderItem item = new FolderItem();
		item.path = category + "/thumbs/" + page;
		item.name = name + ".jpg";
		item.id = item.getId();
		item.directory = item.getDirectory();
		folderItems.add(item);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DailyProducer producer = new DailyProducer();
		
		String[] categories = new String[]{
			"sexy","purity","network","black_yarm","autodyne"
		};
		
		
		//每日更新start
		producer.addItem(getUpdateTime());
		
		for(String category:categories){
			producer.addItemFromCategoryAndPage(category, 2, 21);
			producer.addItemFromCategoryAndPage(category, 2, 22);
		}
		
		//每日更新end
		
		String json = producer.toJson();
		System.out.println(json);
		
		
//		JsonParser parser = new JsonParser() ;
//		JsonArray array = parser.parse(json).getAsJsonArray();
//		producer.getUpdatedTime(array.get(0).getAsString());
//		producer.fromJson(array.get(1));
	}
	
	public static String getUpdateTime(){
		SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm", Locale.CHINA);
		return format.format(Calendar.getInstance().getTime());
	}
	
}
